<?php
include("includes/EmpleadosBackend.php");

?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Inicio de prueba</title>
    <link rel="stylesheet" href="main.css">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous"> 
    <!-- Fontawesome -->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <!-- Datepicker -->
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

</head>
<body>
    <div id="menu">
        <ul>
            <li>Mi Perfil</li>
            <li class="cerrar-sesion"><a href="includes/logout.php">Cerrar sesión</a></li>
        </ul>
    </div>

    <section>
        <h1>Bienvenido <?php echo $user->getNombre();  ?></h1>
    </section>


    <div class="container" style="margin-top: 80px;">
        <h2>Lista de publicaciones</h2>
        <hr>
        <div class="row">
            <div class="col-3">
                <select class="form-control">
                    <option value="0"selected>Filtro de datos Stock</option>
                    <option value="1">Con Stock</option>
                    <option value="2" >Poco Stock</option>
                    <option value="3">Sin Stock</option>
                </select>
            </div>
            <div class="col-3">            
                <button class="btn btn-primary form-control" data-toggle="modal" data-target="#crearNuevoEmpleado">
                    Crear una nueva Publicacion
                </button>
            </div>

        </div>
        <br>
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th>Código Publicador</th>
                    <th>Nombre Publicación</th>
                    <th>Ubicacion Publicación</th>
                    <th>Fecha Publicación</th>
                    <th>Contacto telefónico</th>
                    <th>Descripción</th>
                    <!-- <th>Stock</th> -->
                    <th>Editar</th>
                    <th>Eliminar</th>
                </tr>
                
                <?php
                $contador = 1; 
                
                if(pg_num_rows($consulta)>0){
                    echo "<p>Listado de publicaciones";
                    //echo "---------------------------</p>";
                    while($obj=pg_fetch_object($consulta)){
                        echo "<tr>".
                            "<td>".$obj->p_codigo."</td>".
                            "<td>".$obj->p_nombre."</td>".
                            "<td>".$obj->p_ubicacion."</td>".
                            "<td>".$obj->p_fecha."</td>".
                            "<td>".$obj->p_contacto."</td>".
                            "<td>".$obj->p_descripcion."</td>";
                            ?>

                            </td>
                            <td>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editarEmpleado"><i class="fas fa-edit"></i>
                                </button>
                            </td>
                            <td>
                                <button type="button" class="btn btn-danger"><i class="fa fa-trash"></i>
                                </button>
                            </td>
                <?php    
                    }
                }
                ?>

                <tr>
                    <?php
                    /*
                    if($row['p_estado'] == '1'){
                        echo '<span class="badge badge-success">Con Stock</span>';
                    }else if($row['p_estado'] == '2'){
                        echo '<span class="badge badge-info">Poco Stock</span>';
                    }else if($row['p_estado'] == '3'){
                        echo '<span class="badge badge-warning">Sin Stock</span>';
                    }*/
                    ?>
                    <?php $contador++; ?>
                </tr>
                <?php ?>
            </table>
        </div>
    </div>

    <!-- CrearNuevo Usuario Modal -->
    <div class="modal fade" id="crearNuevoEmpleado" tabindex="-1">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Crear Nueva Publicacion</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <form action="includes/CrearEmpleado.php" method="POST">
                    <div class="row">

                        <div class="form-group col-12">
                            <label>Nombre publicacion</label>
                            <input type="text" class="form-control" name="nombre-publicacion" required>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-6">
                            <label>Descripcion publicacion</label>
                            <input type="text" class="form-control" name="descripcion-publicacion" required>
                        </div> 
                    </div>

                    <div class="row">

                        <div class="form-group col-3">
                            <label>Ubicacion publicacion</label>
                            <input type="text" class="form-control" name="ubicacion-publicacion" required>
                        </div>
                    </div>

                    <div class="row">

                        <div class="form-group col-6">
                                <label>Fecha de publicacion</label>
                                <input type="text" class="form-control" id="datepicker" name="fecha-publicacion" required>
                        </div>
                        
                        <div>
                        <div class="form-group">
                            <label>Estados</label>
                            <select class="form-control" name="estado-publicacion" required>
                                <option value="">Filtro de Stock</option>
                                <option value="1">Con Stock</option>
                                <option value="2">Poco Stock</option>
                                <option value="3">Sin Stock</option>
                            </select>
                        </div>

                    </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-6">
                            <label>Contacto</label>
                            <input type="text" class="form-control" name="contacto-publicacion" required>
                        </div> 
                    </div>


                    <div class="offset-10">
                        <button type="submit" class="btn btn-primary">Enviar</button>
                    </div>
                  </form>
            </div>
        </div>
        </div>
    </div>

    <!-- Editar Empleado Modal -->
    <!--  -->
    <div class="modal fade" id="editarEmpleado" tabindex="-1">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Editar Usuario</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <form action="Modulo/EditarEmpleado.php" method="POST">
                    <div class="row">
                        
                        <div class="form-group col-3">
                            
                            <label>Codigo</label>

                            <input type="text" class="form-control" name="codigo" required>

                        </div>

                        <div class="form-group col-9">
                            <label>Nombre</label>
                            <input type="text" class="form-control" name="nombre" required>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-6">
                            <label>Lugar de nacimiento</label>
                            <input type="text" class="form-control" name="lugar-nacimiento" required>
                        </div>
                            <div class="form-group col-6">
                            <label>Fecha de nacimiento</label>
                        <input type="text" class="form-control" id="datepicker2" name="fecha-nacimiento" required>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-6">
                            <label>Telefono</label>
                            <input type="text" class="form-control" name="telefono" required>
                        </div>
                        <div class="form-group col-6">
                            <label>Cargo</label>
                            <input type="text" class="form-control" name="cargo" required>
                        </div>
                    </div>

                    <div>
                        <div class="form-group">
                            <label>Estados</label>
                            <select class="form-control" name="estado" required>
                                <option value="">Filtro de datos de empleado</option>
                                <option value="1">Fijo</option>
                                <option value="2">Contratado</option>
                                <option value="3">Outsourcing</option>
                            </select>
                        </div>
                    </div>
                    <div class="offset-10">
                        <button type="submit" class="btn btn-primary">Enviar</button>
                    </div>
                  </form>
            </div>
        </div>
        </div>
    </div>


    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <!-- Bootstrap -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
    <!-- Datepicker -->
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    
    <script>
        $('#datepicker').datepicker({
            uiLibrary: 'bootstrap4'
        });

        $('#datepicker2').datepicker({
            uiLibrary: 'bootstrap4'
        });
    </script>

</body>

<script type="module">
  // Import the functions you need from the SDKs you need
  import { initializeApp } from "https://www.gstatic.com/firebasejs/9.2.0/firebase-app.js";
  import { getAnalytics } from "https://www.gstatic.com/firebasejs/9.2.0/firebase-analytics.js";
  // TODO: Add SDKs for Firebase products that you want to use
  // https://firebase.google.com/docs/web/setup#available-libraries

  // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  const firebaseConfig = {
    apiKey: "AIzaSyB2puM_7HsHkTTV-jpR4eFktU2FixT49nM",
    authDomain: "my-project-management-8222b.firebaseapp.com",
    projectId: "my-project-management-8222b",
    storageBucket: "my-project-management-8222b.appspot.com",
    messagingSenderId: "717754624257",
    appId: "1:717754624257:web:6aaa67d7a0f35d0fc01300",
    measurementId: "G-92M0FFK1HX"
  };

  // Initialize Firebase
  const app = initializeApp(firebaseConfig);
  const analytics = getAnalytics(app);
</script>

</html>